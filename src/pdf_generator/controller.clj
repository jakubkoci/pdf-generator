(ns pdf-generator.controller
  (:require [pdf-generator.view :as view]
            [pdf-generator.core :as core]
            [compojure.core :as compojure]
            [ring.adapter.jetty :as jetty]
            [ring.util.response :as response]))

(compojure/defroutes app
  (compojure/GET "/" []
                 (view/home-page))
  (compojure/GET "/pdf/:id" [id]
                 (core/generate-pdf "doc.pdf" id)
                 (response/file-response "doc.pdf")))

(defn -main []
  (prn "View the example at http://localhost:4000/")
  (jetty/run-jetty app {:join? true :port 4000}))
