(ns pdf-generator.dao
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.edn :as edn]))

(def db (edn/read-string (slurp "resources/db-config.edn")))

(defn find-inscriptions [id]
  (map
    (fn [entry] (:content entry))
    (jdbc/query db ["select content from inscription where document_id = ?" id])))
