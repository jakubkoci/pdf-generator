(ns pdf-generator.view
  (:require [hiccup.page :as page]))

(defn home-page []
  (page/html5
    [:head
     [:title "PDF generator"]]
    [:body
     [:a {:href "/pdf/1"} "Generate PDF"]]))
