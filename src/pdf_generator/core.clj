(ns pdf-generator.core
  (:require [pdf-generator.dao :as dao]
            [pdf-generator.generator :as generator])
  (:gen-class))

(defn generate-pdf [filename document-id]
  (let [inscriptions (dao/find-inscriptions document-id)]
    (generator/generate-pdf filename inscriptions)))
