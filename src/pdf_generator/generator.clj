(ns pdf-generator.generator
  (:require [clj-pdf.core :as pdf])
  (:import (java.awt Color Graphics2D GradientPaint BasicStroke)))

(def page-width 1000) ;; in pixels
(def page-height (int (* page-width (/ 210 297))))

(def metadata
  {:title  "PDF document"
   :left-margin   20
   :right-margin  20
   :top-margin    20
   :bottom-margin 20
   :size          [page-height page-width]
   :orientation   :landscape
   :font {:size 14
          :encoding :unicode
          :ttf-name "resources/28 Days Later.ttf"}
   :footer {:page-numbers false}})


(defn paragraphs [inscriptions]
  (map
    (fn [inscription]
      [:paragraph inscription])
    inscriptions))


(def fold-width
  (int (/ page-width 3)))

(def crop-line-margin 10)

(def gradient
  (GradientPaint. 200 100 (Color. 153, 204, 255) 0 1000 (Color. 0 51 153)))

(def dashed
  (BasicStroke. 0.8 BasicStroke/CAP_BUTT BasicStroke/CAP_ROUND 10.0 (float-array [10.0]) 0.0))

(defn graphics-fold-line [start-x start-y end-x end-y]
  [:graphics {:under true}
    (fn [g2d]
      (.setStroke g2d dashed)
      (.setColor g2d Color/BLACK)
      (.drawLine g2d start-x start-y end-x end-y))])

(defn graphics-gradient-background []
   [:graphics {:under true}
    (fn [g2d]
      (.setPaint g2d gradient)
      (.fillRect g2d 0 0 page-width page-height))])

(defn graphics-crop-line [margin]
  [:graphics {:under true}
    (fn [g2d]
      (.setStroke g2d dashed)
      (.setColor g2d Color/BLACK)
      (.drawRect g2d margin margin (- page-width (* margin 2)) (- page-height (* margin 2))))])

(defn generate-pdf [filename inscriptions]
  (pdf/pdf
    [metadata
     (paragraphs inscriptions)
     (graphics-gradient-background)
     (graphics-fold-line fold-width crop-line-margin fold-width (- page-height crop-line-margin))
     (graphics-fold-line (* 2 fold-width) crop-line-margin (* 2 fold-width) (- page-height crop-line-margin))
     (graphics-crop-line crop-line-margin)]
    filename))
