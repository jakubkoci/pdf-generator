(ns pdf-generator.core-test
  (:require [clojure.test :refer :all]
            [pdf-generator.core :refer :all]
            [clojure.java.io :as io]))

(def filename "doc1.pdf")

(deftest generate-new-pdf-file
  (testing "file not exists before"
    (is (= false (.exists (io/file filename)))))
  (testing "file is generated"
    (generate-pdf filename)
    (is (= true (.exists (io/file filename)))))
  (.delete (io/file filename)))
