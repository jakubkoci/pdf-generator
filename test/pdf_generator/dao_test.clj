(ns pdf-generator.dao-test
  (:require [clojure.test :refer :all]
            [pdf-generator.dao :refer :all]))

(deftest retunrs-inscriptions-for-document-with-given-id
  (is (=
        "Tuna burger with marinated onion and French fries"
        (first (find-inscriptions 1)))))
