show databases;
show tables;

create database pdf_generator;
use pdf_generator;

CREATE TABLE IF NOT EXISTS inscription (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	document_id INT UNSIGNED  NOT NULL,
	content VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY (id)
);

INSERT INTO inscription VALUES (null, 1, 'Tuna burger with marinated onion and French fries');
INSERT INTO inscription VALUES (null, 1, 'Boudin blanc with mushroom and barley risotto');
INSERT INTO inscription VALUES (null, 1, 'Seared scallops with cauliflower');
INSERT INTO inscription VALUES (null, 1, 'Potato-truffle gnocchi');
INSERT INTO inscription VALUES (null, 1, 'Poached smoky line caught sea bass');
INSERT INTO inscription VALUES (null, 1, 'Venison loin');
INSERT INTO inscription VALUES (null, 1, 'Beef fillet and braised oxtail');
INSERT INTO inscription VALUES (null, 1, 'Coffee creme brulee');
INSERT INTO inscription VALUES (null, 1, 'Cold and hot Texturas of Valrhona chocolate');
INSERT INTO inscription VALUES (null, 1, 'Cheese cake of Muscat pumpkin');

SELECT * FROM inscription;