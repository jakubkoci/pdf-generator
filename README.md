# Description

Simple PDF generator. When web app starts you can click on the link "Generate PDF". Application takes inscriptions from database, generates PDF file with them and returns that file to download by user.

Database schema is made considering more documents with more inscriptions in them. There is only one document with ID = 1 for the simplicity and testing purposes.

# Installation

Application requires database connection config placed in `resources/db-config.edn` and one database table placed in `resources/pdf-generator.sql`. This file also contains testing data.

# Run

`lein run`
